# Lab -01 

This lab activity aims to provide an overview of fundamental concepts in Object-Oriented Programming, which is a prerequisite to understanding and applying design patterns.


### Tools needed:
- [Java JDK 8 or above.](https://www.oracle.com/java/technologies/downloads/)
- An IDE (e.g., [Apache NetBeans](https://netbeans.apache.org/), [Eclipse](https://www.eclipse.org/) or [IntelliJ IDEA](https://www.jetbrains.com/idea/).
- If you do not like to use an IDE, you may use any text editor (e.g., [VS Code](https://code.visualstudio.com/), [jEdit](http://www.jedit.org/), etc.) and the Javac compiler.


## 1. Instance and class variables

### Count that does not work

Consider the following Java code:

_Product.java_

```java
public class Product {
  private int id;
  private double price;
  private String name;
  private int quantity;
  
  public Product(int id, double price, String name){
    this.id = id;
    this.price = price;
    this.name = name;
    this.quantity ++;
  }
  public void applySaleDiscount(double percentage){
    this.price = this.price - ((percentage/100) * this.price);
  }

  public void addToShoppingCart(){
    System.out.println(this.name + " has been added to the shopping cart.");
  }

  public void printTotalQuantity(){
    System.out.println("Total Quantity: " + this.quantity);
  }
  
  public void getSellableStatus(){
    System.out.println("This product is sellable");
  }

  public String toString(){
    return "Product info:\n+Id: " + this.id + "\t" + "name: " + this.name +
      "\tPrice: SR" + this.price;
  }
}

```

and the following client program:

_App.java_

```java
public class App{

  public static void main(String[]args){
    Product p1 = new Product(6745, 5.50, "Penne Pasta");
    Product p2 = new Product(8853, 6.50, "Spaghetti Pasta");
    Product p3 = new Product(2106, 4.50, "Linguine Pasta");
    p3.printTotalQuantity();
  }
}
```

When compiling this code and running it:

```bash
javac *.java
java App

```

We get the following output:

```
Total Quantity: 1
```

**Question:** How would you fix this code to print the correct total quanitity, 3?



<hr/>



## 2. Inhertiance

Extend the product class by adding the following two derived classes (a.k.a child classes and subclasses):

```java

public class ElectricProduct extends Product{

  private String voltage;

  public ElectricProduct(int id, double price, String name, String voltage){
    super(id, price, name);
    this.voltage = voltage;
  }

  @Override
  public String toString(){
    return super.toString() +"\t Voltage: " + this.voltage;
  }

}
```

```java
import java.time.LocalDate;

public class FoodProduct extends Product{
  private LocalDate expirationDate;

  public FoodProduct(int id, double price, String name, LocalDate expirationDate){
    super(id, price, name);
    this.expirationDate = expirationDate;
  }

  @Override
  public String toString(){
    return super.toString() +"\t Expiration Date: " + this.expirationDate;
  }
}

```


And add the following main/client class:

```java
import java.time.LocalDate;

public class App{

  public static void main(String[]args){
    Product p1 = new Product(6745, 5.50, "Penne Pasta");
    Product p2 = new Product(8853, 6.50, "Spaghetti Pasta");
    Product p3 = new Product(2106, 4.50, "Linguine Pasta");
    FoodProduct p4 = new FoodProduct(3452, 10.0, "Cheddar Cheese", 
        LocalDate.parse("2022-06-07"));
    ElectricProduct p5 = new ElectricProduct(4875, 30.0, "Extension cord", "220v");
  }
}

```

**Question:** The `Product` class should not be instantiated directed. Only concrete classes should be instantiated. What would you do to fix this?

<hr/>

## 3. Polymorphism "Many Forms"
**Question:** Change the main class to utilize the use of Polymorphism  and iterate through an array of `Products` using the enhanced for statement (a.k.a For-Each Loop)?

<hr/>

## 4. Controlling Changes

**Question:** What would you do to prevent subclasses from overriding the `addToShoppingCart()` method of the `Product` class without changing its visibility?

<hr/>

## 5. Abstraction

We decided to add a feature to the `Product` class by adding Order information:

```java
public class Product {
  private int id;
  private double price;
  private String name;
  private int quantity;
  private int orderId;
  private String orderStatus;
  
  public Product(int id, double price, String name, int orderId, String orderStatus){
    this.id = id;
    this.price = price;
    this.name = name;
    this.orderId = orderId;
    this.orderStatus = "created"
    this.quantity ++;
  }
```

**Question:** Why is this considered bad? How would you fix it?

<hr/>

## 6. Encapsulation

We decided to add a feature to the `Product` class by adding the product's `weight` information. We decided to make it public, so any subclass can change it easily.

```java
public class Product {
  private int id;
  private double price;
  private String name;
  public double weight;
  
  public Product(int id, double price, String name, int orderId, String orderStatus){
    this.id = id;
    this.price = price;
    this.name = name;
    this.orderId = orderId;
    this.orderStatus = "created"
    this.quantity ++;
  }
```

**Question:** Why is this considered bad? How would you fix it?

<hr/>

## Submission

Submit your written answers in a PDF file with links to the source code.







